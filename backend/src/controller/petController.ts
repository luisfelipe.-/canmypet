import {deletePetByName, getAllPet, getPetByName, newPet, updatePetByName} from '../repository/petRepository'

export const insertPet = (name:string) =>{
    return newPet(name)
}
export const deletPet = (name:string) =>{
    return deletePetByName(name)
}

export const updatePet = (name:string,newName:string) =>{
    return updatePetByName(name,newName)
}

export const getPets = () =>{
    return getAllPet()
}
export const getAPet = (name:string) =>{
    return getPetByName(name)

}
