import { deleteFoodByName, getAllFood, getFoodByName, newFood, updateFoodByName } from "../repository/foodRepository"

export const insertFood = (name:string, image:string) =>{
    return newFood (name,image)
}
export const deletFood = (name:string) =>{
    return deleteFoodByName(name)
}

export const updateFood = (name:string,newName:string) =>{
    return updateFoodByName(name,newName)
}

export const getFoods = () =>{
    return getAllFood()
}
export const getAFood = (name:string) =>{
    return getFoodByName(name)

}