import {getAllPfoodByPet,getPfoodByPetAndCondition,getPfoodByMascota,getAllPfood, newPFood, deletePfoodByPetAndFood} from '../repository/pfoodRepository'

export const insertPfood = (food:string , pet: string, allow: boolean, description:string) =>{
    return newPFood(food,pet,allow,description)
}
export const getAll = () =>{
    return getAllPfood()
}
export const getPet = (pet:string) =>{
    return getAllPfoodByPet(pet)
}

export const getPetC = (pet:string,condition:boolean) =>{
    return getPfoodByPetAndCondition(pet,condition)
}

export const getPetF = (pet:string,food:string) =>{
    return getPfoodByMascota(pet,food)
}
export const deletePfood = (pet:string,food:string) =>{
    return deletePfoodByPetAndFood(pet,food)
}