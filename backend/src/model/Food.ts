import { getPriority } from "os";
import {Entity,  PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import Pfood from "./Pfood";

@Entity()
export default class Food {
    @PrimaryGeneratedColumn()
    id : number

    @Column()
    name: string 
    
    @Column({nullable:true})
    image: string
    
    @OneToMany((type)=>Pfood,(pfood) => pfood.food)
    pfoods : Pfood[]

}

