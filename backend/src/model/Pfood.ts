import {Entity,  PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn} from "typeorm";
import Pet from './Pet'
import Food from './Food'
@Entity()
export default class Pfood {
    @PrimaryGeneratedColumn()
    id : number

    @Column()
    allow: boolean
    
    @Column("text")
    description: string

    @ManyToOne(type => Pet,{
        eager : true
    })
    @JoinColumn()
    pet: Pet

    @ManyToOne(type => Food,{
        eager : true
    })
    @JoinColumn()
    food: Food
    
}