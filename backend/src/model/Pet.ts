import {Entity,  PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import Pfood from "./Pfood";

@Entity()
export default class Pet {
    @PrimaryGeneratedColumn()
    id : Number

    @Column()
    name: string 

    @OneToMany((type)=>Pfood,(pfood) => pfood.pet)
    pfoods : Pfood[]

}