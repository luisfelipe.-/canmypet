import "reflect-metadata"
import Pet from '../model/Pet'
import { getRepository,getManager} from "typeorm"

export const newPet = async(_name : string) => {
    const newpet = new Pet()
    newpet.name = _name
    await getRepository(Pet).save(newpet)
    return{
        successfull : true,
        message: 'Food inserted correctly'
    }
}

export const getAllPet = async () => {
    const pets = await getRepository(Pet).find()
    if(!pets){
        return {
            succesfull : false,
            message: 'There are not pets registered'
        }
    }
    return {
        succesfull : true,
        pets
    }
    
}
export const getPetByName = async (_name : string) => {
    const pet = await getRepository(Pet).findOne({name : _name})
    if(!pet){
        return {
            succesfull : false,
            message: 'doesn´t exist a pet with this name'
        }
    }

    return {
        succesfull : true,
        pet
    }
    
}

export const updatePetByName = async (_name : string,_newname : string) => {
    const pet = await getRepository(Pet).update({name : _name},{name : _newname})
    if(!pet.affected){
        return {
            succesfull : false,
        }
    }
    return {
        succesfull : true,
    }
    
}

export const deletePetByName = async (_name : string) => {
    const pet = await getRepository(Pet).delete({name : _name})
    if(!pet.affected){
        return {
            succesfull : false,
            message: 'doesn´t registered a food with this name'
        }
    }
    return {
        succesfull : true,
        message: 'deleted this food'
    }
    
}