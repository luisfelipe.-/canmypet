import "reflect-metadata"
import Food from '../model/Food'
import { getRepository,getManager} from "typeorm"

export const newFood = async(_name : string, _image : string) => {
    const newfood = new Food()
    newfood.name = _name
    await getRepository(Food).save(newfood)
    return{
        successfull : true,
        message: 'Food inserted correctly'
    }
}

export const getAllFood = async () => {
    const foods = await getRepository(Food).find()
    if(!foods){
        return {
            succesfull : false,
            message: 'There are not foods registered'
        }
    }
    return {
        succesfull : true,
        foods
    }
    
}
export const getFoodByName = async (_name : string) => {
    const food = await getRepository(Food).findOne({name : _name})
    if(!food){
        return {
            succesfull : false,
            message: 'doesn´t exist a food with this name'
        }
    }

    return {
        succesfull : true,
        food
    }
    
}
export const updateFoodByName = async (_name : string,_newname : string) => {
    const food = await getRepository(Food).update({name : _name},{name : _newname})
    if(!food.affected){
        return {
            succesfull : false,
        }
    }
    return {
        succesfull : true,
    }
    
}

export const deleteFoodByName = async (_name : string) => {
    const food = await getRepository(Food).delete({name : _name})
    if(!food.affected){
        return {
            succesfull : false,
            message: 'doesn´t registered a food with this name'
        }
    }
    return {
        succesfull : true,
        message: 'deleted this food'
    }
    
}