import "reflect-metadata"
import Pfood from "../model/Pfood"

import * as PetRepository from './petRepository'
import * as FoodRepository from './foodRepository'
import { getRepository,getManager} from "typeorm"


export const newPFood = async(_nameFood : string, _namePet: string,_allow: boolean, _description : string) => {
    const _pet = (await PetRepository.getPetByName(_namePet)).pet
    const _food = (await FoodRepository.getFoodByName(_nameFood)).food
    
    const pfood = new Pfood()
    
    pfood.allow = _allow
    pfood.description = _description
    pfood.pet = _pet
    pfood.food = _food

    const save = await getRepository(Pfood).save(pfood)
    return{
        successfull : true,
        message: 'Relation inserted correctly'
    }
}
export const getAllPfood = async () => {
    const pfoods = await getRepository(Pfood).find()
    if(pfoods.length == 0){
        return {
            succesfull : false,
            message: 'There are not relations registered'
        }
    }
    return {
        succesfull : true,
        pfoods
    }
    
}

export const getAllPfoodByPet = async (_namePet : string) => {
    const _Pet = (await PetRepository.getPetByName(_namePet)).pet
    const pfoods = await getRepository(Pfood).find({pet : _Pet})
    
    if(pfoods.length == 0){
        return {
            succesfull : false,
            message: 'doesn´t exist a relation with this pet'
        }
    }

    return {
        succesfull : true,
        pfoods
    }
    
}
export const getPfoodByPetAndCondition = async (_namePet : string, _can : boolean) => {
    const _Pet = (await PetRepository.getPetByName(_namePet)).pet
    const results = await getRepository(Pfood).find({pet: _Pet,allow : _can})
    if(results.length == 0){
        return {
            succesfull : false,
            message: 'doesn´t exist a relation with this pet'
        }
    }
    return {
        succesfull : true,
        results
    }
}


export const getPfoodByMascota = async (_namePet : string, _nameFood : string) => {
    const _Pet = (await PetRepository.getPetByName(_namePet)).pet
    const _Food = (await FoodRepository.getFoodByName(_nameFood)).food
    const pfood = await getRepository(Pfood).findOne({food : _Food,pet : _Pet})
    if(!pfood){
        return {
            succesfull : false,
            message: 'This pet hasn´t registered a relation with this food'
        }
    }
    return {
        succesfull : true,
        pfood
    }
    
}
export const deletePfoodByPetAndFood = async (_namePet : string,_nameFood:string) => {
    const _Pet = (await PetRepository.getPetByName(_namePet)).pet
    const _Food = (await FoodRepository.getFoodByName(_nameFood)).food
    const pet = await getRepository(Pfood).delete({pet : _Pet, food:_Food})
    if(!pet.affected){
        return {
            succesfull : false,
            message: 'doesn´t registered a relation with this name'
        }
    }
    return {
        succesfull : true,
        message: 'deleted this relation'
    }
    
}

