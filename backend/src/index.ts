import express, { Express } from 'express'
import bodyParser from 'body-parser'
import { createConnection } from 'typeorm'

// Controllers
import {insertPet } from './controller/petController'
import {insertFood} from './controller/foodController'
import {insertPfood,getPetC, getPetF, getPet, getAll} from './controller/pfoodController'
import { getAllPet } from './repository/petRepository'

var cors = require('cors')

const app: Express = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(cors())
createConnection().then(
    () => console.log('Conexion creada')
).catch(
    error => console.log('Error de conexion', error)
)

// Services

//Inicio
app.get('/', async(req,res) =>{
    res.json({ message: 'App status OK, index' })
})
//////////////////Insertar

//Insertar mascota
app.post('/insert/pet', async(req,res)=>{
    const response = await insertPet(req.body.name)
    res.json(response)

})

//Insertar Alimento
app.post('/insert/food', async(req,res)=>{
    const response = await insertFood(req.body.name,req.body.image)
    res.json(response)

})

//Insertar Relacion
app.post('/insert/pfood', async(req,res)=>{
    const response = await insertPfood(req.body.food,req.body.pet,Boolean(Number(req.body.allow)),req.body.description)
    res.json(response)

})

//////////////Busqueda

//Buscar todas las relaciones mascota-alimento
app.get('/search', async (req,res) =>{
    const response = await getAll()
    res.json(response)

})
//buscar las relaciones mascota-alimento de una mascota en especifico
app.get('/search/:pet', async (req,res) =>{
    const response = await getPet(req.params.pet)
    res.json(response)

})

//Buscar si una relacion mascota-alimento puede/no puede
app.get('/search/allow/:pet/:condition', async (req,res) =>{
    const response = await getPetC(req.params.pet, Boolean(Number(req.params.condition)))
    res.json(response)

})

//Buscar una relacion especifica mascota-alimento
app.get('/search/one/:pet/:food', async (req,res) =>{
    const response = await getPetF(req.params.pet,req.params.food)
    res.json(response)

})


app.listen(4000, () => {
    console.log('Server listening at port 4000')
})
