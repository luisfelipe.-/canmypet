import SearchForm from '../components/searchPetCondition.component'
import { useState } from 'react'
import DataPetCondition from '../components/dataPetCondition.component'
import {Container} from 'react-bootstrap'
function SearchByPetAndCondition (props) {
    const [petName, setSearchName] = useState("")
    const [condition, setCondition] = useState(false)
    const [searchState,setSearchState] = useState(false)    
    return (
        <div>
            <SearchForm setSearchName = {setSearchName} setCondition = {setCondition} setSearch={setSearchState}/>
            <Container>
                <DataPetCondition petName = {petName} condition = {condition} isSearch={searchState} setSearch={setSearchState}/>
            </Container>
        </div>
    )
}

export default SearchByPetAndCondition


