import NavbarSearch from '../components/navbar.component'
import { useState } from 'react'
import Show from './show.layout';
import { Container } from 'react-bootstrap';
function InfoPanel () {
    const [componenteView, setComponenteView] = useState(0)

    return (
        <div>
            <NavbarSearch  stateView={componenteView} setView={setComponenteView}/>
            <Show stateView={componenteView}/>

        </div>
    )
 
}

export default InfoPanel


