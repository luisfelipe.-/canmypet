import {Button, Container,Form,FormControl, Row, Col} from 'react-bootstrap'
import {useState} from 'react'
import axios from 'axios'
import DataPetByPet from '../components/dataPet.component'
function SearchByPet (props) {
    const [pet,SetPet] = useState("")
    const [datos,SetDatos] = useState([])
    const handlePetChange = (event) =>{
        SetPet(event.target.value)
    }
    const handleClick = (event) =>{
        if(pet !== ""){
            axios.get('http://localhost:4000/search/'+ pet)
            .then(response=>{
                SetDatos(response.data)
            })
        }
    }
    return (
        <div>
            
            <Container>
                <Form.Group>
                    <Form.Label>Ingrese Animal a buscar</Form.Label>
                    <Form.Control type= "text" placeholder= "Nombre animal" onChange={handlePetChange} />
                </Form.Group>
                <Button variant="warning" onClick = {handleClick} >Buscar</Button>
            </Container>


            <Container>
                <DataPetByPet datos = {datos}/>
            </Container>
        </div>
    )
 
}

export default SearchByPet
