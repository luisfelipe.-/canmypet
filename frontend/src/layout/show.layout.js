import SearchByPet from './searchByPet.layout'
import SearchByPetAndCondition from './searchByPetAndCondition.layout'
import SearchByPetAndFood from './searchPetFood.layout'
import {Container, Image, Row,Col} from 'react-bootstrap'
import './layout.css'

function Sample (props) {
    if(props.stateView === 1){
        return (
            <div>           
                <SearchByPetAndCondition stateView={props.stateView}/>
            </div>
        )
    }else if(props.stateView === 2){
        return (
            <div>           
                <SearchByPet stateView={props.stateView}/>
            </div>
        )
    }else if(props.stateView === 3){
        return (
            <div>           
                <SearchByPetAndFood stateView={props.stateView}/>
            </div>
        )
    }
    else{
        return (
            <div className="mt-0">  
                <Image src ="https://drive.google.com/uc?export=view&id=1Bff-sp-g3toa58AlEbUs759IDhZ8-o-U" fluid />
                <Container className="mt-5" border ="danger">
                    <Row>
                        <Col xs={6}>
                            <h3 className="sample-h3 mb-5">Can My Pet? Es una aplicación desarrollada para poder consultar que alimentos se nos permite dar a nuestras mascotas. Además de brindar argumentos y consejos sobre el por qué, ó no  alimentar a nuestas mascotas con cada producto, se mostrará una imagen para cada uno, a manera de dar a conocer cómo se ven generalmente, lo que permitirá reconocerlo fácilmente.</h3>
                        </Col>
                        <Col xs={5} md={4}>
                            <Image src="https://drive.google.com/uc?export=view&id=1oxYSmqVXxTo7n6FJ_5MjKTGTk33veJrw" className = "mr-3"/>
                        </Col>
                    </Row>
                    
                </Container>
            </div>
        )
    }

}

export default Sample
