import SearchForm from '../components/searchPetFood.component'
import { useState } from 'react'
import DataPetFood from '../components/dataPetFood.component'
function SearchPetFood(props){
    const [petName, setPetName] = useState("")
    const [food, setFoodName] = useState("")
    const [searchState,setSearchState] = useState(false)

    return (
        <div>

            <SearchForm setPetName = {setPetName} setFoodName = {setFoodName} setSearch={setSearchState}/>
            <DataPetFood petName = {petName} food = {food} isSearch={searchState} setSearch={setSearchState} />
        

        </div>
    )
}
export default SearchPetFood