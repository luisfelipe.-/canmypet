import axios from 'axios'
import { useState } from 'react'
import {Card,Row,Col,Image, Tooltip, OverlayTrigger, Container, Alert} from "react-bootstrap"
function DataPetCondition(props){
    const [petData,setPetData] = useState([])
    const fetchData = async () =>{
        axios.get('http://localhost:4000/search/allow/' + props.petName + '/' + props.condition

        ).then(res => {setPetData(res.data.results)})
            
    }
    function color(condition){
        if(condition){
            return "success"
        }else{
            return "danger"
        }
    }
    function SetTooltip(condition){
        if(condition){
            return(<Tooltip id="button-tooltip-2"><strong>Puede</strong> comer</Tooltip>)
        }else{
            return(<Tooltip id="button-tooltip-2"><strong>No Puede</strong> comer</Tooltip>)
        }
    }

    if(props.petName && props.condition){
        if(props.isSearch){
            fetchData()
            props.setSearch(false)
        }    
        if(petData){
            return (
                <div>
                    <Row className="mt-5">
                        {
                            petData.map((pfood)=>
                                <Col md={4} key={pfood.id}>
                                    <OverlayTrigger placement="right" delay={{ show: 150, hide: 250 }} overlay={SetTooltip(pfood.allow)}>
                                        <Card  className="ml-3 mt-5 mr-3" bg={color(pfood.allow)} text='white'>
                                            <Card.Header>
                                                <Image src ={pfood.food.image} className="card-img-top"/>
                                            </Card.Header>
                                            <Card.Body >
                                                <Card.Title>
                                                    {pfood.food.name}
                                                </Card.Title>
                                                <Card.Text>
                                                    {pfood.allow}
                                                </Card.Text>
                                                <Card.Text>
                                                    {pfood.description}
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                </OverlayTrigger>
                                </Col>
                            )
                        }
                    </Row>
                </div> 
            )
        }
        else{
            return(
                <div>
                <br></br>
                <Container>
                    <Alert  class="alert badalert-color" variant="light">
                        <p>                  
                            Oh! No contamos con ese animal por el momento.
                            <br></br>
                            Puedes enviarnos un correo a canmypet@gmail.com
                            solicitando que ingresemos esta mascota a nuestra plataforma
                        </p>
                    </Alert>
                </Container>

                </div>
            )
        }

    }
    else{
        return(

            <Container>
                            <br></br>
                <Alert  class="alert alert-color" variant="success">
                    Ingrese nombre de un animal y seleccione una opcion.
                </Alert>
            </Container>

        )
    }
}
export default DataPetCondition
