import { Container } from "react-bootstrap"
import {Card,Row,Col,Image, Tooltip, OverlayTrigger, Alert} from "react-bootstrap"
import './components.css'
function DataPetByPet(props){
    function color(condition){
        if(condition){
            return "success"
        }else{
            return "danger"
        }
    }
    function SetTooltip(condition){
        if(condition){
            return(<Tooltip id="button-tooltip-2"><strong>Puede</strong> comer</Tooltip>)
        }else{
            return(<Tooltip id="button-tooltip-2"><strong>No Puede</strong> comer</Tooltip>)
        }
    }

    if(props.datos.length !== 0){
        if(props.datos.succesfull === true){
            return (
                <div>
                    <Row className="mt-5">
                        {
                            props.datos.pfoods.map((pfood)=>
                                <Col md={4} key={pfood.id}>
                                    <OverlayTrigger placement="right" delay={{ show: 150, hide: 250 }} overlay={SetTooltip(pfood.allow)}>
                                        <Card  className="ml-3 mt-5 mr-3" bg={color(pfood.allow)} text='white'>
                                            <Card.Header>
                                                <Image src ={pfood.food.image} className="card-img-top"/>
                                            </Card.Header>
                                            <Card.Body >
                                                <Card.Title>
                                                    {pfood.food.name}
                                                </Card.Title>
                                                <Card.Text>
                                                    {pfood.allow}
                                                </Card.Text>
                                                <Card.Text>
                                                    {pfood.description}
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                   </OverlayTrigger>
                                </Col>
                            )
                        }
                    </Row>
                </div> 
            )
        }else{
            return(
                <div>
                <br></br>
                <Container>
                    <Alert  class="alert badalert-color" variant="light">
                        <p>                  
                            Oh! No contamos con ese animal por el momento.
                            <br></br>
                            Puedes enviarnos un correo a canmypet@gmail.com
                            solicitando que ingresemos esta mascota a nuestra plataforma
                        </p>
                    </Alert>
                </Container>

                </div>
            )
        }
    }else{
        return(
            <div>
                <br></br>
                <Container>
                    <Alert  class="alert alert-color" variant="success">
                        Ingrese nombre de un animal
                    </Alert>
                </Container>

            </div>

        )
    }

}
export default DataPetByPet

/*import axios from 'axios'
import { useState } from 'react'
import {Card,Row,Col} from "react-bootstrap"
function DataPetCondition(props){
    const [petData,setPetData] = useState([])
    const fetchData = async () =>{
        axios.get('http://localhost:4000/search/allow/' + props.petName + '/' + props.condition

        ).then(res => {setPetData(res.data.results)})

    }
    if(props.petName && props.condition){
        fetchData()
        if(petData){
            return (

                <div>
                    <Row className="mt-5">
                        {
                            petData.map((food)=>
                                <Col md={3} key={food.id}>
                                    <Card  className="ml-3 mt-5 mr-3" border="danger">
                                        <Card.Header>
                                        {/* <Image src ={"https://"+product.image} className="card-img-top"/> }
                                        </Card.Header>
                                        <Card.Body >
                                            <Card.Title>
                                                {food.food.name}
                                            </Card.Title>
                                            <Card.Text>
                                                <p>
                                                    {food.description}
                                                </p>
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        }
                    </Row>
                    </div> 
            )
        }
        else{
            return(null)
        }

    }
    else{
        return(null)
    }
}
export default DataPetCondition
*/