import { Form, Button } from 'react-bootstrap'
import { useState } from 'react'
import { Container } from 'react-bootstrap'
function SearchForm(props){
    const [searchInput, setSearchInput] = useState("")
    const [selectCondition,setSelectCondition] = useState(false)
    const handleSelect=(e) =>{
        setSelectCondition(e.target.value)
    }
    function FormFunct(){
        props.setSearchName(searchInput)
        props.setCondition(selectCondition)
        props.setSearch(true)
    }
    

   return(
    <div>
        <Container>
            <Form.Group>
                <Form.Label>Ingrese nombre</Form.Label>
                <Form.Control type= "text" placeholder= "Nombre animal" value={searchInput} onChange={(value)=>{setSearchInput(value.target.value)}} />

            </Form.Group>
            <Form.Group>
                <Form.Label>Seleccione una opcion</Form.Label>
                <Form.Control as="select" onChange= {handleSelect}>
                    <option disabled="disabled" selected="true">Elija una opcion</option>
                    <option value = "1">Comidas que puede comer</option>
                    <option value = "0">Comidas que no puede comer</option>
                </Form.Control>
            </Form.Group>
            <Button variant="warning" onClick={FormFunct}>Buscar</Button>
        </Container>
    </div>
   )
}

export default SearchForm
