import axios from 'axios'
import { useState } from 'react'
import {Card,Container,Image, Tooltip, OverlayTrigger, Spinner, Alert,Col,Row} from "react-bootstrap"
function DataPetFood(props){
    const [petData,setPetData] = useState([])
    const fetchData = async () =>{
        axios.get('http://localhost:4000/search/one/' + props.petName + '/' + props.food)
        .then(res => {
            setPetData([res.data])
            props.setSearch(false)
        })
    }
    function color(condition){
        if(condition){
            return "success"
        }else{
            return "danger"
        }
    }
    function SetTooltip(condition){
        if(condition){
            return(<Tooltip id="button-tooltip-2"><strong>Puede</strong> comer</Tooltip>)
        }else{
            return(<Tooltip id="button-tooltip-2"><strong>No Puede</strong> comer</Tooltip>)
        }
    }
            
    if(props.food && props.petName){
        if(props.isSearch){
            fetchData()
        }   
        if(petData.length !== 0){
            if(petData[0].succesfull){
                return(
                <Row className="justify-content-md-center">
                    <Col xs={4} className="mb-5 mt-2">
                            <OverlayTrigger placement="right" delay={{ show: 150, hide: 250 }} overlay={SetTooltip(petData[0].pfood.allow)}>
                                <Card bg={color(petData[0].pfood.allow)} text='white' style={{width: "100%" }}>
                                    <Card.Header>
                                        <Image src ={petData[0].pfood.food.image} className="card-img-top"/>
                                    </Card.Header>
                                    <Card.Body >
                                        <Card.Title>
                                            {petData[0].pfood.food.name}
                                        </Card.Title>
                                        <Card.Text>
                                            {petData[0].pfood.allow}
                                        </Card.Text>
                                        <Card.Text>
                                            {petData[0].pfood.description}
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </OverlayTrigger>
                        </Col>
                    </Row>
                )
            }else{
                return(
                    <div>
                        <br></br>
                        <Container>
                            <Alert  class="alert badalert-color" variant="light">
                                <p>                  
                                    Oh! De momento no contamos con esta información.
                                    <br></br>
                                Puedes enviarnos un correo a canmypet@gmail.com
                                solicitando que sea ingresada a nuestra plataforma.
                            </p>
                            </Alert>
                        </Container>
    
                    </div>
                )   
            }
        }else{
            return(
                <div>
                    <br></br>
                    <Spinner animation="border" variant="success" />
                </div>

            )
        }
    }else{
        return(
            <Container>
                <br></br>
                <Alert  class="alert alert-color" variant="success">
                    Ingrese nombre de un animal e ingrese el nombre de una comida.
                </Alert>
            </Container>
        )
    }
}   

export default DataPetFood
