import { Navbar, Nav} from 'react-bootstrap'
import './components.css'



function NavbarSearch(props){
    
    return(
        <div>
            <Navbar className= "nav-color" variant="light">
                <Navbar.Brand onClick={()=>{props.setView(0)}}><img src="https://drive.google.com/uc?export=view&id=1P0ye2mvHE2_qgN52RP8W9RXMkf9DGZqo" width="50" height="50" className="d-inline-block align-top"/>Can my pet?</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link onClick={()=>{props.setView(2)}}>Comidas para mi mascota</Nav.Link>
                    <Nav.Link onClick={()=>{props.setView(1)}}>Filtrar comidas por mascota</Nav.Link>
                    <Nav.Link onClick={()=>{props.setView(3)}}>Puede mi mascota comer...?</Nav.Link>
                </Nav>

            </Navbar>

        </div>
    )
}

export default NavbarSearch


