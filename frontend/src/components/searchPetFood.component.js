import { Form, Button } from 'react-bootstrap'
import { useState } from 'react'
import { Container } from 'react-bootstrap'
function SearchForm(props){
    const [nameInput, setNameInput] = useState("")
    const [foodInput,setFoodInput] = useState("")

    function FormFunct(){
        props.setPetName(nameInput)
        props.setFoodName(foodInput)
        props.setSearch(true)
    }
    

   return(
    <div>
        <Container>
            <Form.Group>
                <Form.Label>Ingrese nombre</Form.Label>
                <Form.Control type= "text" placeholder= "Nombre animal" value={nameInput} onChange={(value)=>{setNameInput(value.target.value)}} />

            </Form.Group>
            <Form.Group>
            <Form.Label>Ingrese alimento</Form.Label>
            <Form.Control type= "text" placeholder= "Nombre Alimento" value={foodInput} onChange={(value)=>{setFoodInput(value.target.value)}} />

            </Form.Group>
            <Button variant="warning" onClick={FormFunct}>Buscar</Button>
        </Container>
    </div>
   )
}

export default SearchForm
